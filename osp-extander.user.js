// ==UserScript==
// @name     OSP Extander
// @version  1
// @grant    none
// ==/UserScript==

// To da się przyśpieszyć, trzeba zexpolitować "pakiety"

var arrayWithAllButtonsIds = [
    "calculation-protection-cover-CANCER-ONB",
    "calculation-protection-cover-USZCZ",
    "calculation-protection-cover-SOC",
    "calculation-protection-cover-SOW",
    "calculation-protection-cover-POW-CH",
    "calculation-protection-cover-DX12",
    "calculation-protection-cover-AD12",
    "calculation-protection-cover-AT12",
    "calculation-protection-cover-CN12",
    "calculation-protection-cover-PD12",
    "calculation-protection-cover-TP12",
    "calculation-protection-cover-WP12"
  ];
  
  function OspExtanderTurnOffAllExtraContracts() {
    let starterTimeout = 2000;  
    
        arrayWithAllButtonsIds.forEach(button => {
        setTimeout(() => {
            let tempButton = document.getElementById(button);
          if(tempButton.classList.contains("active")){
              tempButton.click();
          }
        }, starterTimeout);
        starterTimeout += 2000;
      });
        
  }
  
  function OspExtanderAddButton(extraContractsColumn){
  //   console.log(extraContractsColumn)
           let newButton = document.createElement("button");
    
         newButton.innerHTML = "Wyłącz wszystkie";
        newButton.style.backgroundColor = "red";
    
    
        newButton.onclick = OspExtanderTurnOffAllExtraContracts;
   
        // Dodaj guzik
        extraContractsColumn.insertBefore(newButton, extraContractsColumn.childNodes[0])
  //   temp.appendChild(newButton)
  //   temp.insertAfter(newButton, temp.firstChild);
  }
  
  
  
  function OspExtandermyIntervalFunc(){
    
  //   let el = document.getElementById("calculation-protection-cover-CANCER-ONB");
  //   console.log(el);
  //   el.click();
    
    let probablyColumnWithContracts = document.getElementsByClassName('col-xs-12 covers-header ng-binding');
    if(probablyColumnWithContracts.length !== 0){
      // Jeśli jakieś kolumny zostały znalezione, oznacza to że strona się załadowała i należy wyłączyć Interval
         clearInterval(interval);
      
      // Tej sieczki nie rozumiem za bardzo
      OspExtanderAddButton(probablyColumnWithContracts[0].parentNode.nextSibling.nextElementSibling.parentNode);
      
    }  
  }
  
  let interval = setInterval(OspExtandermyIntervalFunc, 2000);
  
  